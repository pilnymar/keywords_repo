# Use me ONLY in emergency
## Labels
#label1 #label2 #label4 #label5
## Description
If the office is on fire, use it.
## Code
```python
def emergency():
    sys.exit()

```