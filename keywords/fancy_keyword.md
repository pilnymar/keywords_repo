# Fancy keyword
## Labels
#label1 #label2 #label3
## Description
Really fancy keyword. Make sure to use it in project.
## Code
```java
void fancy(int a){
    System.out.println(a+5);
}
```