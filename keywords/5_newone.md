# New one

## Description

Brand new keyword to rule them all.

link: https://gitlab.fit.cvut.cz/pilnymar/keywords_repo/issues/5

## Code
```java
int newOne(){
	return callAwesome() + callLegendary() + 5;
}

```
