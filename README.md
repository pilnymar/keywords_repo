# keywords_repo
## Research
### Gitlab
#### Issues
* Vestavěné labely.
* Fulltext vyhledávání (i v popisu/kódu keywordu).
* Related issues (Gitlab 12.8 a vyšší).
* Lze zaznamenat vývoj v komentářích dané issue.

#### .md soubory
* Verzování přes commity/merge requesty.
* Labely dané issue jako plain text (pro filtrování nutnost fulltext).
* Fulltext vyhledávání by šlo s [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html) (netestováno).
* Related issues možná přes odkazy unitř keywordu (popř. adresářovou strukturu).

### Jetbrains Space
#### .md soubory na gitu
* Navíc možnsot fulltext vyhledávání bez Elasticsearch.

#### Issues
* Není možnsot filtrovat pomocí více tagů (=labelů).
* Není možnost fulltext vyhledávání v popisu keywordu.

## Workflows
### Nový keyword
1. Založení issue (Gitlab/Jira)
    * Popis
    * Aktualni kód
    * Labely/Tagy
    * Typ issue = Technical task
2. Vytvořit soubor v masteru v /keywords/X_nazev_keywordu.md (X je ID issue)
    * Popis
    * Link na issue
    * Aktualni kód
3. Commit s nazvem "[#X] Init" (aby se to prolinkovalo s issue)
4. Do popisu issue přidat link na soubor
### Úprava keywordu
1. Nová branch update_X
    * Commity začínající "[#X] "
2. MR z update_X do masteru (opět začínající "[#X] ")
    * Squash commits (a delete source branch?)
3. Aktualizovat popis a aktualni kód u issue